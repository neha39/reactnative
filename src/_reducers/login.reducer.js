import { loginConstants } from "../_constants/login.constants";

const initialState = {};
export function login_reducers(state = initialState, action) {
    switch (action.type) {
        case loginConstants.REDUX_ADD_STORAGE:
        case loginConstants.LOGIN_USER_ACTION_SUCCESS:
            return {
                loggedIn: true,
                user: action.payload,
            };
        case loginConstants.LOGOUT_USER_ACTION_SUCCESS:
            return {loggedIn: false, user: {}}
        default: 
            return state
    }
}