import { combineReducers } from "redux";

import { login_reducers } from './login.reducer';

const rootReducer = combineReducers({
    login_reducers,
});
  
export default rootReducer;