import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { baseUrl } from "./urls";

let Authorization = "";

const getToken = async () => {
  Authorization = await AsyncStorage.getItem("jwt");
};

const axiosInstance = axios.create({
    baseURL: baseUrl
});

axiosInstance.interceptors.request.use(
    async function (config) {
        await getToken();
        const Access_Token = Authorization ? Authorization : '';

        Access_Token && Access_Token.length ? config.headers.Authorization  = `Bearer ${Access_Token}` : null
        // you can also do other modification in config
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

axiosInstance.interceptors.response.use(
    (response) => {
        if (response.status === 401) {
            console.log('401')
        }
        return response;
    },
    (error) => {
        if (
            error.response &&
            error.response.data &&
            error.response.data.status === 401
        ) {
            AsyncStorage.removeItem("user");
            AsyncStorage.removeItem("jwt");
            return Promise.reject(error.response && error.response.data);
        }
        return Promise.reject(error.response.data);
    }
);
export default axiosInstance;