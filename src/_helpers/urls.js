import Config from "react-native-config"

export const baseUrl = Config.ROOT_URL


export const auth = {
    login: '/users/rest-auth/login/',
    logout: '/users/rest-auth/logout/',
}