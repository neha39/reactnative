import AsyncStorage from "@react-native-async-storage/async-storage";
import { loginConstants } from "../_constants/login.constants";
import axiosInstance from "../_helpers/axiosInstance";


export const login_user_action = (data) => async (dispatch) => {
    dispatch({
      type: loginConstants.LOGIN_USER_ACTION_REQUEST,
    });
    try {
      const response = await axiosInstance.post(auth.login, data);
      if (response.status) {
        AsyncStorage.setItem("jwt", response.data.access_token);
        AsyncStorage.setItem("user", JSON.stringify(response.data));
        dispatch({
          type: loginConstants.LOGIN_USER_ACTION_SUCCESS,
          payload: response.data,
        });
        return response;
      } else {
        return false;
      }
    } catch (e) {
    
      dispatch({
        type: loginConstants.LOGIN_USER_ACTION_FAILURE,
        payload: e
      });
    }
  };
