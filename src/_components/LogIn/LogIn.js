import React from 'react';
import { connect } from "react-redux";
import { View, Text, Button, TextInput , TouchableOpacity } from 'react-native';
import { login_user_action } from "../../_actions/login.actions";
import { validateEmail } from "../../_helpers/validators";

class LoginScreen extends React.Component {

  state = {
    email: "",
    password: "",
    errorText: "",
    isApiLoading: false,
  }
  handleChange = (name, value) => {
    this.setState({ [name]: value }, () => {
      if (name === "email") {
        const isValidEmail = validateEmail(this.state.email);
        if (!isValidEmail) {
          this.setState((prevState) => ({
            inputError: {
              ...prevState.inputError,
              emailError: "Enter a valid email!",
            },
          }));
          return;
        }
      }

      if (!this.state[name].length) {
        this.setState((prevState) => ({
          inputError: {
            ...prevState.inputError,
            [name === "email"
              ? "emailError"
              : "passwordError"]: `This field is required!`,
          },
        }));
      } else {
        this.setState((prevState) => ({
          inputError: {
            ...prevState.inputError,
            [name === "email" ? "emailError" : "passwordError"]: ``,
          },
        }));
      }
    });
  };

  onLogin = async () => {
    const { email, password, inputError } = this.state;

    // Keyboard.dismiss()

    if (
      !email.length ||
      (inputError.emailError && inputError.emailError.length)
    ) {
      this.setState((prevState) => ({
        inputError: {
          ...prevState.inputError,
          emailError: "Email is Required!",
        },
      }));
      return;
    }

    if (
      !password.length ||
      (inputError.passwordError && inputError.passwordError.length)
    ) {
      this.setState((prevState) => ({
        inputError: {
          ...prevState.inputError,
          passwordError: "Password is Required!",
        },
      }));
      return;
    }

    const data = { email, password };
    this.setState({ isApiLoading: true });
    // await this.props.onSigningUser(data);
    this.setState({ isApiLoading: false });
  };
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>LoginScreen</Text>
        <TextInput
          underlineColorAndroid="transparent"
          placeholder="Email"
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          onSubmitEditing = {this.handleChange}
        />
        <TextInput
          underlineColorAndroid="transparent"
          placeholder="Password"
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          onSubmitEditing = {this.handleChange}        />

        <TouchableOpacity  onPress={this.onLogin} >
          <Text>login</Text>
          </TouchableOpacity>
      </View>
    );
  }

};

const mapStateToProps = (state) => {
  return {
    error: state.error,
    user: state,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onSigningUser: (data) => dispatch(login_user_action(data)),
});


export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);