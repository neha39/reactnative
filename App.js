import React from 'react'
import LoginScreen from './src/_components/LogIn/LogIn'     
import { Provider } from 'react-redux'    
import  store from './src/_helpers/store'

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store} >    
        <LoginScreen />    
      </Provider>    
    )
  }
}
